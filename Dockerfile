FROM williamyeh/ansible:ubuntu16.04-onbuild

# File Author / Maintainer
MAINTAINER Jørgen Blakstad

# Update apt
RUN apt-get update
RUN apt-get install -y vim git zlib1g-dev libxml2-dev libxslt-dev python-dev

# Install pip
RUN easy_install pip

# Configure ansible
RUN perl -pi -e 's/#inventory/inventory/g' /etc/ansible/ansible.cfg
RUN perl -pi -e 's/#library/library/g' /etc/ansible/ansible.cfg
RUN mkdir -p /usr/share/my_modules/

# Install ntc-ansible
RUN cd /usr/share/my_modules/ && git clone https://github.com/networktocode/ntc-ansible --recursive
RUN pip install ntc-ansible
RUN pip install terminal

# Install napalm
RUN cd /usr/share/my_modules/ && git clone https://github.com/napalm-automation/napalm-ansible.git
RUN pip install napalm

# Install jedha
RUN pip install --upgrade jedha

# Fix ssh
RUN mkdir /root/.ssh
RUN touch /root/.ssh/known_hosts

ENTRYPOINT bash